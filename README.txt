Readme file for the password edit module
---------------------------------------------

This is a Drupal 7 module, it provides a password field, then anonymous users can create/edit the node with the field.
For login users, the password filed would automatically disappear.

Installation:
  Installation is like with all normal drupal modules:
  1. extract the 'password_edit' folder from the tar ball to the
     modules directory from your website (typically sites/all/modules).
  2. add field 'Password to edit' to your content type, this filed would only show for anonymous users, 
     you should enable the create/edit permissions of the content type to anonymous user.
  
Dependencies:
  none.

Conflicts/known issues:
  this field can't set to be required in content type, we add a validation for empty instead.

Configuration:
  none.
  
